class Game {
  constructor() {
    this.field = document.querySelector(".field");
    this.emptySquare = {
      value: 16,
      top: 3,
      left: 3,
    };
    this.squares = [this.emptySquare];
    this.numbers = [...Array(15).keys()]
      .map((x) => x + 1)
      .sort(() => Math.random() - 0.5);
  }

  move(index) {
    const square = this.squares[index + 1];
    const leftDiff = Math.abs(this.emptySquare.left - square.left);
    const topDiff = Math.abs(this.emptySquare.top - square.top);
    if (leftDiff + topDiff > 1) {
      return;
    }
    square.elem.style.left = `${this.emptySquare.left * 100}px`;
    square.elem.style.top = `${this.emptySquare.top * 100}px`;
    const emptyLeft = this.emptySquare.left;
    const emptyTop = this.emptySquare.top;
    this.emptySquare.left = square.left;
    this.emptySquare.top = square.top;
    square.left = emptyLeft;
    square.top = emptyTop;
    if (this.isWin()) {
      alert("YOU WIN!");
      window.location.reload();
    }
  }

  isWin() {
    let isWin = this.squares.every((square) => {
      return square.value === square.top * 4 + square.left + 1;
    });
    return isWin;
  }

  start() {
    for (let i = 0; i <= 14; i++) {
      const square = document.createElement("div");
      square.className = "square";
      const value = this.numbers[i];
      square.innerHTML = value;
      const left = i % 4;
      const top = (i - left) / 4;
      this.squares.push({
        value: value,
        left: left,
        top: top,
        elem: square,
      });

      square.style.left = `${left * 100}px`;
      square.style.top = `${top * 100}px`;
      this.field.append(square);
      square.addEventListener("click", () => {
        this.move(i);
      });
    }
  }
}
let newGame = new Game().start();
